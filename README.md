## Language

[Kotlin](https://kotlinlang.org/)


## Features
*   **Offline support:** the App caches any visited list (categories and search) into the room database.
*   Discover the most popular, most rated & upcoming movies
*   Search for movies by title
*   User can view and play trailers on youtube 
*   Shows a list of reviews for each movie
*   Shows general info for each movie
*   MVVM with Android Architecture Components(Room, LiveData, ViewModel)
*   Databinding,BindingAdapters
*   Pagination and endless scrolling using custom pagination.
*   Handle network status and network failures
*   ConstraintLayout(barriers... etc)
*   Material design.


## Libraries

-   [AndroidX](https://developer.android.com/jetpack/androidx/) - Previously known as 'Android support Library'
-   [Glide](https://github.com/bumptech/glide) - for loading and caching images 
-   [Retrofit 2](https://github.com/square/retrofit) - Type-safe HTTP client for Android and Java by Square, Inc. 
-   [Gson](https://github.com/google/gson) - for serialization/deserialization Java Objects into JSON and back
-   [Koin](https://insert-koin.io/) - for dependency injection
- [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/) 
    * [Room](https://developer.android.com/topic/libraries/architecture/room)
    * [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
    * [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
- [Android Data Binding](https://developer.android.com/topic/libraries/data-binding/) 
- [OkHttp](https://github.com/square/okhttp)
- [CircleImageView](https://github.com/hdodenhof/CircleImageView)
- [Material Design](https://material.io/develop/)
